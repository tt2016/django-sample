from datetime import datetime

from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.
def hello_world(request):
    return HttpResponse('Hello World')

def hello_template(request):
    
    time = {
            'hour': datetime.now().hour,
            'message': 'Sample message',
            }
    
    return render(request, 'index.html', time)