<p>＊システム概要<p>
　python/djangoのサンプルプログラム。<br>
　<br>
　　実装機能：　<br>
　　ー画面遷移　<br>
　　ーD/B更新（登録、更新、削除）　<br>
　　ーロギング　<br>
　　ー自動テスト（unittest）　<br>
　<br>
<p>＊動作環境<p>
　<br>
　python 3.5.2　<br>
　django 1.10　<br>