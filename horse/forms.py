from django import forms


class HorseForm(forms.Form):
    bamei = forms.CharField(
        label='馬名',
        max_length=255,
        required=True,
        widget=forms.TextInput()
    )
    
    barei = forms.IntegerField(
        label='馬齢',
        min_value=1,
        max_value=99,
        required=True,
    )
    
    class_cd = forms.IntegerField(
        label='クラス',
        min_value=1,
        max_value=10,
        required=True,
    )
    