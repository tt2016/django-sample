from django.db import models

# Create your models here.
class Horse(models.Model):
    bamei = models.CharField(max_length=255)
    barei = models.IntegerField(default=1)
    class_cd = models.IntegerField(default=1)
    sex = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u"{0}:{1}... ".format(self.id, self.bamei)