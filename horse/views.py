from django.shortcuts import (
    render, 
    redirect, 
    get_object_or_404,
    ) 
from django.http import HttpResponse
from .models import Horse
from .forms import HorseForm  
from django.views.decorators.http import require_POST
import logging


# Create your views here.
def index(request):
    
    logger = logging.getLogger('command')
    logger.debug("test")
    
    d = {
        'horses': Horse.objects.all(),
    }
    return render(request, 'horse/index.html', d)

def add(request):
    form = HorseForm(request.POST or None)
    if form.is_valid():
        Horse.objects.create(**form.cleaned_data)
        return redirect('horse:index')

    d = {
        'form': form,
    }
    return render(request, 'horse/edit.html', d)

def edit(request, editing_id):
    horse = get_object_or_404(Horse, id=editing_id)
    if request.method == 'POST':
        form = HorseForm(request.POST)
        if form.is_valid():
            horse.bamei = form.cleaned_data['bamei']
            horse.save()
            return redirect('horse:index')
    else:
        logger = logging.getLogger('command')
        logger.debug("edit")
        logger.debug(horse.bamei)
        form = HorseForm({'bamei': horse.bamei,
                          'barei': horse.barei,
                          'class_cd': horse.class_cd,
                          })
    d = {
        'form': form,
    }
    
    return render(request, 'horse/edit.html', d)

@require_POST
def delete(request):
    delete_ids = request.POST.getlist('delete_ids')
    if delete_ids:
        Horse.objects.filter(id__in=delete_ids).delete()
    return redirect('horse:index')    
