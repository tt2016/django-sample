from django.test import TestCase
from .models import Horse

# Create your tests here.
class SampleTest(TestCase):
    def test_model_horse(self):
        
        horse_count = Horse.objects.all().count()
        
        horse = Horse()
        bamei, barei, class_cd, sex = 'テスト', 1, 2, 3
        horse.bamei = bamei
        horse.barei = barei
        horse.class_cd = class_cd
        horse.sex = sex
        horse.save()
        
        saved_horses = Horse.objects.all()
        actual_horse = saved_horses[horse_count]
        
        self.assertEqual(actual_horse.bamei, bamei)  
        self.assertEqual(actual_horse.barei, barei)     
        self.assertEqual(actual_horse.class_cd, class_cd) 
        self.assertEqual(actual_horse.sex, 3) 
         